#include<iostream>
#include<memory>

class myClass{
	private:
		int val;
	public:
		myClass() = delete;
		myClass(int val) :val{val}{std::cout<<"myClass("<<val<<") constructed\n";}
		~myClass() {std::cout<<"myClass("<<val<<") deleted\n";}
		int get() const {return val;}
};

bool ifEqualMake(int a, int b, std::unique_ptr<myClass> &obj)
{
	if(a == b) {
		obj = std::unique_ptr<myClass>(new myClass(a));
	}
	return(a==b);
}

int main()
{
	std::unique_ptr<myClass> obj1, obj2, obj3;

  ifEqualMake(3, 3, obj1);
  ifEqualMake(4, 3, obj2);
  ifEqualMake(4, 4, obj3);

	if(obj1.get()) std::cout<<"obj 1 made in heap: "<<obj1->get()<<'\n';
	if(obj2.get()) std::cout<<"obj 2 made in heap: "<<obj2->get()<<'\n'; 
	if(obj3.get()) std::cout<<"obj 3 made in heap: "<<obj3->get()<<'\n';

	// No need to delete. No memory leak

	return 0;

}
