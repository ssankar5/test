#include<iostream>

class myClass{
	private:
		int val;
	public:
		myClass() = delete;
		myClass(int val) :val{val}{std::cout<<"myClass("<<val<<") constructed\n";}
		~myClass() {std::cout<<"myClass("<<val<<") deleted\n";}
		int get() const {return val;}
};

bool ifEqualMake(int a, int b, myClass *& obj)
{
	if(a == b) {
		obj = new myClass(a);
	}
	return(a==b);
}

int main()
{
	myClass* obj1 = nullptr;
	myClass* obj2 = nullptr;
	myClass* obj3 = nullptr;


  ifEqualMake(3, 3, obj1);
  ifEqualMake(4, 3, obj2);
  ifEqualMake(4, 4, obj3);

	if(obj1) std::cout<<"obj 1 made in heap: "<<obj1->get()<<'\n';
	if(obj2) std::cout<<"obj 2 made in heap: "<<obj2->get()<<'\n'; 
	if(obj3) std::cout<<"obj 3 made in heap: "<<obj3->get()<<'\n';

	if(obj1) delete obj1;
	if(obj2) delete obj2; // deleting without checking if(obj) might have been error
	if(obj3) delete obj3;

	return 0;

}
