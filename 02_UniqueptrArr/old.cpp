#include <iostream>
#include <memory>

class myComplex {
private:
  double real{0}, imag{0};

public:
  myComplex(double r, double i = 0) : real{r}, imag{i} {
    std::cout << "Constructed " << real << " + " << imag << "i\n";
  }
  double getReal() { return real; }
  double getImaginary() { return imag; }
  myComplex &setReal(double r) {
    real = r;
    return *this;
  }
  myComplex &setImaginary(double i) {
    imag = i;
    return *this;
  }
  ~myComplex() { std::cout << "Destructed " << real << " + " << imag << "i\n"; }
};

int main() {
  auto compl_nums = new myComplex[10]{1, 3, 4, 1, 54, 2, 15, 2, 3, {4, -3}};
  for (int i = 0; i < 10; ++i) {
    compl_nums[i].setReal(i + 0.5).setImaginary(i * 1.0 + 1);
  }


  delete[] compl_nums; // This line is never reached
}
