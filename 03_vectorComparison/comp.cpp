#include <iostream>
#include <vector>

using namespace std;

int main() {
  vector<vector<int>> VV;
  VV.push_back({0, 0});
  VV.push_back({1, 4, 2, 5});
  VV.push_back({4, 2, 5});
  VV.push_back({1, 2, 8});
  VV.push_back({6, 2, 5});
  VV.push_back({8, 4, 1});

  vector<int> s{1, 4, 2, 5};

  for (auto &v : VV) {
    cout << "New vector: ";
    for (auto &e : v)
      cout << e << " ";
    if (s == v)
      cout << " Found it!";
    else
      cout << " No luck here";
    cout << '\n';
  }

  return 0;
}
