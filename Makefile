echo: 
	@echo "Making ..."

test: run00 run03 run02


run00:
	cd 00_Uniqueptr
	make
	cd ..

run01:
	cd 01_Sharedptr
	make
	cd ..

run 02:
	cd 02_UniqueptrArr
	make
	cd ..

run03:
	cd 03_vectorComparison
	make
	cd ..
